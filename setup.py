from setuptools import setup, find_packages

setup(
    name="Pylodie",
    version="1.0",
    packages=find_packages(),
    scripts=['__main__.py'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['docutils>=0.3'],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
    },

    # metadata to display on PyPI
    author="Adil Benameur, Clotaire Dufresne, Félix Courtemanche",
    author_email="adil.benameur1@gmail.com",
    description="Pylodie",
    project_urls={
        "Source Code": "https://bitbucket.org/adilbenameur/pylodie"
    }
)