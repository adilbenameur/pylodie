#pylint: disable=no-name-in-module,no-member
from PyQt5.QtWidgets import QAction, QFileDialog
from PyQt5.QtGui import QIcon
import pickle
from pylodie.func.processingAudio.getNotes import getNotes
from pylodie.func.processingAudio.getBpm import getBpm

class settingUpTopMenu():
    def __init__(self):
        self.iconesPath = 'pylodie/img/icones'
        self.menuBar = self.menuBar()
        self.tempo = 0
        self.notes = None

    def settingUpTopMenu(self, setSelfNotes):
        ### Setting up menu bar on top
        self.setSelfNotes = setSelfNotes
        self.openFile = QAction(QIcon('{}/load.png'.format(self.iconesPath)), 'Import audio file', self)
        self.openFile.triggered.connect(self.openFileMethod)
        fileMenu = self.menuBar.addMenu('File')
        fileMenu.addAction(self.openFile)

        self.saveFile = QAction(QIcon('{}/save.png'.format(self.iconesPath)), 'Save partition file', self)
        self.saveFile.triggered.connect(self.saveFileTab)
        fileMenu.addAction(self.saveFile)

    def openFileMethod(self):
        fileToOpen = QFileDialog()
        self.audioFilePath = fileToOpen.getOpenFileName(self, "Open File")[0]
        if self.audioFilePath != "":
            if self.audioFilePath[len(self.audioFilePath)-4:] == '.tab':
                audioFile = open(self.audioFilePath, 'rb')
                load = pickle.load(audioFile)
                self.notes = load['notes']
                self.setSelfNotes(load['notes'])
                self.tempo = load['tempo']
                audioFile.close()
            else:
                self.setFilePath(self.audioFilePath)
                self.notes = getNotes(self.audioFilePath, 5)
                self.setSelfNotes(getNotes(self.audioFilePath, 5))
                self.tempo = getBpm(self.audioFilePath)
            self.update()
        else:
            self.update()

    def saveFileTab(self):
        directoryToSaveIn = QFileDialog()
        fileToSaveIn = directoryToSaveIn.getSaveFileName(self, "Save file")[0]
        try:
            if self.notes != None:
                save = open(fileToSaveIn, 'wb')
                toDump = {'notes': self.notes, 'tempo': self.tempo}
                pickle.dump(toDump, save)
                save.close()
        except Exception:
            pass

    # Getter fonction
    def getTempo(self):
        return self.tempo

    def getFilePath(self):
        return self.audioFilePath