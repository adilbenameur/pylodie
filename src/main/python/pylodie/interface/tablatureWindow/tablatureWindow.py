#pylint: disable=no-member, import-error, no-name-in-module
#import PyQt dependencies
from PyQt5.QtWidgets import QWidget, QFileDialog
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtCore import Qt
import math


#import other libs
import aubio
import pickle
import queue

# import pylodie dependencies
from pylodie.func.processingAudio.getBpm import getBpm
from pylodie.func.processingAudio.getNotes import getNotes
from pylodie.func.processingAudio.traduction import traduction

class Tablature(QWidget):
    def __init__(self):
        super().__init__()
        
		# Variable contenant respectivement la note et le temps de début de celle-ci
        self.notes = None

		# Variable contenant le chemin d'accès au fichier audio
        self.audioFilePath = ''

		# Variable contenant le numero de la page actuel
        self.numeroDeDefilement = 0

    def resizeEvent(self, event):
        """
            Fonction appelée lors du redimensionnement de la fenêtre et permettant d'appeler
            à son tour la fonction paintEvent
        """
        self.paintEvent()
        

    def paintEvent(self, event=None):
        """
            Fonction appelée lors du redimensionnement de la fenêtre ou de l'appel de la fonction update
            Permet l'affichage d'une tablature vierge adaptée à la taile de la fenêtre du widget
        """
        
		# Initialisation du pinceau
        pinceau = QPainter(self)

		# Initialisation d'un conteneur de propriétés (encre = "noire", épaisseur = 1)
        proprietesPinceau = QPen(Qt.black)
        proprietesPinceau.setWidth(1)
        
		# Application des propriétés au pinceau
        pinceau.setPen(proprietesPinceau)

		# Codage de la mise en forme dans des variables
        self.espaceInterligne = 15
        self.tailleLigneTablature = 5*15
        self.espaceEntreLignesTab = 40
        self.margeHaute = 10
        
		# Marge de longueur pour la première ligne de tablature
        self.margeGauche1 = 30
        self.margeDroite1 = 10
        
		# Marge de longueur pour toutes les autres lignes de tablature
        self.margeGauche_ = 10
        self.margeDroite_ = 10
        


		# Calcul du nombre de ligne de tablature possible par rapport à la taille de la fenêtre du widget
        self.nombreDeLignePossible = self.calculNombreDeLigne()
        
		# Affichage des lignes de la tablature
        for numeroDeLigne in range(self.nombreDeLignePossible):
            for numeroInterligne in range(6):
                xInterligne = numeroDeLigne * (self.espaceEntreLignesTab + self.tailleLigneTablature) + numeroInterligne * self.espaceInterligne + self.margeHaute

		# Paint une ligne par rapport à deux point respectivement x1, y1, x2, y2
                pinceau.drawLine(0, xInterligne, self.width(), xInterligne)



		# Nombre de X possible par rapport à la taille de la fenêtre du widget
        self.xPossible = self.calculXPossible()

		# Calcul du décalage: nombre de x à ajouter pour passer à la page suivante
        decalage = 0
        for i in range(self.numeroDeDefilement):
            if i <= self.nombreDeLignePossible:
                decalage += self.xPossible



		# Affiche TAB au pour la première ligne et pour la première page de la tablature
        if decalage == 0:
            
		# Affichage d'une ligne verticale pour la première ligne et pour la première page de la tablature
            pinceau.drawLine(0, self.margeHaute, 0, self.tailleLigneTablature + self.margeHaute)
            
		# Création d'un conteneur de propriété pour la police d'écriture
            police = pinceau.font()
            police.setPixelSize(28)
            
		# Appllication des propriétés de police au pinceau
            pinceau.setFont(police)
            
		# Marge lettre
            margeGaucheLettre = 5
            margeHauteLettre = 24
            pinceau.drawText(margeGaucheLettre,margeHauteLettre+self.margeHaute,"T")
            pinceau.drawText(margeGaucheLettre,margeHauteLettre*2+self.margeHaute,"A")
            pinceau.drawText(margeGaucheLettre,margeHauteLettre*3+self.margeHaute,"B")
        
		# Affichage des notes sur la tablature
        policeNote = pinceau.font()
        policeNote.setPixelSize(14)
        pinceau.setFont(policeNote)



		# Position du premier x à 30 : comprend la marge pour le 'TAB'
        if decalage == 0:
            x = self.margeGauche1
        else:
            x = self.margeGauche_

		# Numero de la ligne de la tablature traité dans la boucle
        numeroDeLigne = 0
        margeHauteNumeroAffiche = 15

		# Si un fichier audio a été chargé
        if self.notes != None:
            
		    # Vérifie que décalage (numéro de note au départ) est supérieur ou égale au nombre de note dans self.note
            while decalage <= len(self.notes)-1 and numeroDeLigne <= self.nombreDeLignePossible:
                
		        # Vérifie que décalage est supérieur ou égale au nombre de note dans self.note
                while x < self.width()-10 and decalage <= len(self.notes)-1:
                    
		            # Traduit la note repectivement en numéro d'interligne (en partant du bas) et en numéro à afficher
                    trad = traduction(self.notes[decalage][0])
                    
		            # Vérifie que la note est traduisible
                    if trad != None:
                        
		                # Transforme le numéro d'interligne partant du bas en partant du haut
                        numeroInterligneNote = 6 - trad[0]
                        
		                # Numéro à afficher
                        noteToPaint = trad[1]
                        y = (self.tailleLigneTablature + self.espaceEntreLignesTab) * numeroDeLigne + numeroInterligneNote * self.espaceInterligne + margeHauteNumeroAffiche
                        
                        
		                # Changement temporaire des propriétés du pinceau
                        pinceau.setPen(QPen(Qt.white))
                        
		                # Affiche une petite ligne blanche derrière le numero de la note pour améliorer la visibilité
                        pinceau.drawLine(x-3,y-5,x+(3+8*(len(str(noteToPaint)))),y-5)
                        
		                # Réinitialisation des propriétés du pinceau
                        pinceau.setPen(QPen(Qt.black))
                        
		                # Affichage du numéro de la note
                        pinceau.drawText(x,y,str(noteToPaint))
                    
		            # Ajoute l'intervalle entre les notes
                    x += 40
                    
		            # Ajoute une note au compteur de note
                    decalage += 1
                
		        # Ajoute une ligne au compteur de ligne
                numeroDeLigne += 1
                
		        # Rénisialisation de la marge pour les lignes suivantes
                x = self.margeDroite_


    # Fonction nécessaire au fonctionnement de la class Tablature
    def calculNombreDeLigne(self):    
	    # Comptage des lignes possibles par rapport à la taille de la fenêtre du widget, égale à self.height()
        hauteur = self.height() - self.margeHaute

	    # hauteur widget moins marge haute
        nombreDeLigne = 0
        while True:
	        
            # Vérifie que la hauteur est supérieur à la taille d'une ligne de tablature
            if hauteur-self.tailleLigneTablature >= 0:

	            # Soustrait la taille d'une ligne
                hauteur -= self.tailleLigneTablature
                nombreDeLigne += 1
                if hauteur-self.espaceEntreLignesTab >= 0:
                    
                    # Soustrait la taille d'un espace entre les lignes
                    hauteur -= self.espaceEntreLignesTab
            else:
                break
        return nombreDeLigne


    def calculXPossible(self):
		# Calcul du nombre de note possible par rapport à la taille de la fenêtre du widget
        if self.notes != None:
            xPossible = 0
            xPosition = 30
            numeroDeLigne = 1
            while xPossible <= len(self.notes) and numeroDeLigne <= self.nombreDeLignePossible:
                while xPosition < self.width()-10:
                    xPosition += 40
                    xPossible += 1
                xPosition = 10
                numeroDeLigne += 1
            return xPossible



    # Setter
    def setDefilementNumPlus(self, numeroDeDefilement):
        if self.notes != None:
            self.numeroDeDefilement += 1
            if self.numeroDeDefilement > self.nombreDeLignePossible:
                self.numeroDeDefilement = self.nombreDeLignePossible
        self.update()


    def setDefilementNumMoins(self, numeroDeDefilement):
        if self.notes != None:
            self.numeroDeDefilement -= 1
            if self.numeroDeDefilement < 0:
                self.numeroDeDefilement = 0
        self.update()


    def setSelfNotes(self, notes):
        self.notes = notes
        self.update()


    def setFilePath(self, path):
        self.audioFilePath = path



    # Getter
    def getFilePath(self):
        return self.audioFilePath


    def getTempo(self):
        if self.audioFilePath != '':
            if self.audioFilePath[len(self.audioFilePath)-4:] == '.tab':
                return self.tempo
            else:
                return getBpm(self.audioFilePath)
        else:
            return 0