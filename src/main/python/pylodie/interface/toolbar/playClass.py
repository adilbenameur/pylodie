#pylint: disable=no-name-in-module,no-member
from PyQt5.QtCore import QThread
from PyQt5.QtGui import QIcon
from pygame import mixer
from pylodie.func.processingAudio.getNotes import getNotes

class playClass():
    def __init__(self):
        super().__init__()
        self.playPauseIcones = ['{}/play.png'.format(self.iconesPath), '{}/pause.png'.format(self.iconesPath)]
        self.playPauseCount = 0
        self.playRecord = None

    def playMethod(self):
        if self.filePath != None:
            self.playPauseCount = (self.playPauseCount + 1) % 2
            if self.playPauseCount == 0:
                self.play.setIcon(QIcon(self.playPauseIcones[self.playPauseCount]))
                self.play.setStatusTip('Lecture')
                self.playRecord.quit()
            else:
                self.play.setIcon(QIcon(self.playPauseIcones[self.playPauseCount]))
                self.play.setStatusTip('Pause')
                self.playRecord = PlayAudioFileThreaded(self.filePath)
                self.playRecord.start()


class PlayAudioFileThreaded(QThread):
    def __init__(self, filePath):
        super().__init__()
        self.filePath = filePath

    def quit(self):
        self.mixer.music.stop()

    def run(self):
        self.mixer = mixer
        self.mixer.init()
        self.mixer.music.load(self.filePath)
        self.mixer.music.play()