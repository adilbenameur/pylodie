# Appel des dépendances de PyQt
#pylint: disable=no-name-in-module,no-member, import-error
from PyQt5.QtWidgets import (
    QMainWindow, QWidget, QApplication,
    QVBoxLayout, QHBoxLayout, QLabel, QSizePolicy
    )
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt
import sys


# Import les sous-interfaces de Pylodie
from pylodie.interface.tablatureWindow.tablatureWindow import Tablature
from pylodie.interface.tempoWidget.tempoWidget import TempoWidget
from pylodie.interface.toolbar.settingUpToolbarButton import settingUpToolbarButton
from pylodie.interface.settingUpTopMenu.settingUpTopMenu import settingUpTopMenu
from pylodie.interface.subwindowTabToolbar.subwindowTabToolbar import subwindowTabToolbar


class Pylodie(QMainWindow, settingUpToolbarButton, settingUpTopMenu, subwindowTabToolbar):
    def __init__(self, screenGeometry):
        super().__init__()

        # Initialisation de la sous-fenêtre de la tablature
        tablaturePaint = Tablature()

        # Initialisation de l'affichage du tempo
        tempoPaint = TempoWidget(self.getTempo)

        # Initialisation du menu (sauvegarde et chargement)
        # Argument = fonction permettant le transfert des notes chargées par un fichier
        #            à la class tablaturePaint
        self.settingUpTopMenu(tablaturePaint.setSelfNotes)

        tablatureToolbar = self.addToolBar("tablature toolbar")
        self.settingUpToolbarButton(tablaturePaint.setSelfNotes, self.getFilePath, tablaturePaint.setDefilementNumPlus, tablaturePaint.setDefilementNumMoins)
        tablatureToolbar.addAction(self.record)
        tablatureToolbar.addAction(self.play)
        tablatureToolbar.addAction(self.defilementGauche)
        tablatureToolbar.addAction(self.defilementDroite)
        tablatureToolbar.setMovable(False)
        

        windowToolbar = self.addToolBar("resize window toolbar")
        self.settingUpSubwindowTabToolbar(screenGeometry)
        windowToolbar.addAction(self.haut)
        windowToolbar.addAction(self.gauche)
        windowToolbar.addAction(self.droite)
        windowToolbar.addAction(self.bas)
        windowToolbar.setMovable(False)


        w = QWidget()
        w.setGeometry(10, 10, 10, 10)
        image = QLabel(w)
        pixmap = QPixmap('pylodie/img/icones/note.png')
        pixmap = pixmap.scaled(40, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        image.setPixmap(pixmap)

        tempoBox = QHBoxLayout()
        tempoBox.setAlignment(Qt.AlignLeft)
        tempoBox.addWidget(w, stretch=1)
        tempoBox.addWidget(tempoPaint, stretch=10)
        
        
        mainSubwindow = QVBoxLayout()
        mainSubwindow.addLayout(tempoBox, stretch=1)
        mainSubwindow.addWidget(tablaturePaint, stretch=10)

        w = QWidget()
        w.setLayout(mainSubwindow)
        w.setStyleSheet('border:0px; background-color: rgb(255,255,255);')
        self.setCentralWidget(w)

        ### Setting up window background (white)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)

        self.setFixedSize(515, 585)
        self.setWindowTitle('Pylodie')
        self.setWindowIcon(QIcon('icones/pylodie-logo-v2.png'))
        self.show()

logosPath = 'pylodie/img/logo'
iconsPath = 'pylodie/img/icones'


app = QApplication(sys.argv)
app.setWindowIcon(QIcon('{}/pylodie-logo-v2.png'.format(logosPath)))
ex = Pylodie(screenGeometry = app.desktop().screenGeometry())
sys.exit(app.exec_())