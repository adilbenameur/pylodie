import sounddevice as sd

def listMic():
    devices = list(sd.query_devices())
    inputDevices = []
    
    for device in devices:
        if device['max_input_channels'] > 0:
            inputDevices.append(device)
    return inputDevices