def traduction(x):
    """
        Fonction de traduction des notes reçues par aubio en valeurs utilisables
        pour la tablature : [x,y] avec x la ligne de la note en partant du bas
        et y le numéro à afficher
    """
    #dictionnaire servant à la comparaison de la note
    trad={"E3": [1,0], "F3": [1,1], "F#3": [1,2], "G3": [1,3],"A4": [2,0],
    "A#4": [2,1], "B4": [2,2],"C4": [2,3], "C#4": [2,4],"D4": [3,0], "D#4": [3,1],
    "E4": [3,2],"F4": [3,3],"F#4": [3,4], "G4": [4,0], "G#4": [4,1], "A5": [4,2],
    "A#5": [4,3], "B5": [5,0], "C5": [5,1], "C#5": [5,2], "D5": [5,3], "D#5": [5,4],
    "E5": [6,0], "F5": [6,1], "F#5": [6,2], "G5": [6,3], "G#5": [6,4], "A6": [6,5],
    "A#6": [6,6], "B6": [6,7], "C6": [6,8], "C#6": [6,9], "D6": [6,10], "D#6": [6,11],
    "E6": [6,12], "F6": [6,13], "F#6": [6,14], "G6": [6,15], "G#6": [6,16] }
    try:
        return trad[x]
    except KeyError:
        return None