# pylint: disable=no-name-in-module
from aubio import source, tempo
from numpy import median, diff
from math import ceil

def getBpm(path):
    s = source(path)
    o = tempo("specdiff")
    beats = []


    while True:
        samples, read = s()
        is_beat = o(samples)
        if is_beat:
            this_beat = o.get_last_s()
            beats.append(this_beat)
        if read < 512:
            break


    if len(beats) > 1:
        bpms = 60./diff(beats)
        b = median(bpms)
    else:
        b = 0
    return ceil(b)