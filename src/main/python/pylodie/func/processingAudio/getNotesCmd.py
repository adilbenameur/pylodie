from subprocess import check_output
import aubio
import math

def getNotesCmd(filePath, decimalNumber=6): # note, timeStart, timeEnd
    output = str(check_output(["aubio", "notes", "-i", filePath]))[2:-3].rsplit('\\n')
    noteStartEnd = []

    for note in output:
        if len(note[:-2].rsplit('\\t')) == 3:
            noteStartEnd.append([aubio.midi2note(int(float(note.rsplit('\\t')[0])))] + [truncateFloat(float(note.rsplit('\\t')[1]), decimalNumber)] + [truncateFloat(float(note.rsplit('\\t')[2]), decimalNumber)])

    # For returning the intervals
    for i in range(len(noteStartEnd)-1):
        if i < len(noteStartEnd):
            truncateFloat(noteStartEnd[i+1][1]-noteStartEnd[i][1], 2)

    return noteStartEnd

def truncateFloat(number, decimalNumber, doRound=True):
    if doRound:
        return math.ceil(number * 10 ** decimalNumber) / 10 ** decimalNumber
    else:
        return int(number * 10 ** decimalNumber) / 10 ** decimalNumber


if __name__ == "__main__":
    # For testing
    print(getNotesCmd("/Users/adilbenameur/Downloads/6mgg8y24.mp3", decimalNumber=6))