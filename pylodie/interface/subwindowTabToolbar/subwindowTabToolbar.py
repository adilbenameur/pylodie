#pylint: disable=no-name-in-module,no-member
from PyQt5.QtWidgets import QAction, QWidget
from PyQt5.QtGui import QIcon


class subwindowTabToolbar():
    def __init__(self):
        super().__init__()
        self.iconesPath = 'pylodie/img/icones'

    def settingUpSubwindowTabToolbar(self, screenGeometry):
        self.screenHeight = screenGeometry.height()
        self.screenWidth = screenGeometry.width()

        self.bas = QAction(QIcon('{}/fleche_bas.png'.format(self.iconesPath)), 'Record', self)
        self.bas.setShortcut('Ctrl+R')
        self.bas.triggered.connect(self.basMethod)

        self.haut = QAction(QIcon('{}/fleche_haut.png'.format(self.iconesPath)), 'Play', self)
        self.haut.setShortcut('Ctrl+Q')
        self.haut.triggered.connect(self.hautMethod)

        self.gauche = QAction(QIcon('{}/fleche_gauche.png'.format(self.iconesPath)), 'Play', self)
        self.gauche.setShortcut('Ctrl+Q')
        self.gauche.triggered.connect(self.gaucheMethod)

        self.droite = QAction(QIcon('{}/fleche_droite.png'.format(self.iconesPath)), 'Play', self)
        self.droite.setShortcut('Ctrl+Q')
        self.droite.triggered.connect(self.droiteMethod)

    def basMethod(self):
        if self.height()+115 <= self.screenHeight:
            self.setFixedHeight(self.height()+115)
        
    def hautMethod(self):
        if self.height()-115 >= 585:
            self.setFixedHeight(self.height()-115)
    
    def droiteMethod(self):
        if self.width()+40*4 <= self.screenWidth:
            self.setFixedWidth(self.width()+40*4)

    def gaucheMethod(self):
        if self.width()-40*4 >= 355:
            self.setFixedWidth(self.width()-40*4)