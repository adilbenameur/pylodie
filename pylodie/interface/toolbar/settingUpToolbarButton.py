#pylint: disable=no-name-in-module,no-member
from PyQt5.QtWidgets import QAction, QWidget
from PyQt5.QtGui import QIcon
from pylodie.interface.toolbar.recordClass import recordClass
from pylodie.interface.toolbar.playClass import playClass


class settingUpToolbarButton(playClass, recordClass):
    def __init__(self):
        super().__init__()
        self.iconesPath = 'pylodie/img/icones'
        self.defilementNum = 0
        self.filePath = None

    def settingUpToolbarButton(self, setNotes, getFilePath, setDefilementNumPlus, setDefilementNumMoins):
        self.setNotes = setNotes
        self.getFilePath = getFilePath

        self.record = QAction(QIcon('{}/record.png'.format(self.iconesPath)), 'Record', self)
        self.record.setStatusTip('Enregistrer')
        self.record.triggered.connect(self.recordMethod)

        self.play = QAction(QIcon('{}/play.png'.format(self.iconesPath)), 'Play', self)
        self.play.setStatusTip('Lecture')
        self.play.triggered.connect(self.playMethod)

        self.defilementDroite = QAction(QIcon('{}/fleche_defilement_droite.png'.format(self.iconesPath)), 'Play', self)
        self.defilementDroite.setStatusTip('Défilement Droite')
        self.defilementDroite.triggered.connect(setDefilementNumPlus)

        self.defilementGauche = QAction(QIcon('{}/fleche_defilement_gauche.png'.format(self.iconesPath)), 'Play', self)
        self.defilementGauche.setStatusTip('Défilement Gauche')
        self.defilementGauche.triggered.connect(setDefilementNumMoins)

    def setFilePath(self, setFilePath):
        self.filePath = setFilePath