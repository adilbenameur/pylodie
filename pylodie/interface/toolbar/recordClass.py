#pylint: disable=no-name-in-module,no-member,import-error
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QLabel, QListWidget, QDialogButtonBox, QAction
from PyQt5.QtGui import QPainter, QCloseEvent
from PyQt5.QtCore import QRect, QMetaObject, Qt
import sys

# import pylodie function
from pylodie.func.micro.listMic import listMic
from pylodie.func.micro.startRecord import ThreadedRecording as TR

class recordClass():
    def __init__(self):
        super().__init__()
        self.iconesPath = 'pylodie/img/icones'
        self.recordStopCount = 0
        self.recordStopIcones = ['{}/record.png'.format(self.iconesPath), '{}/stop.png'.format(self.iconesPath)]
        self.mic = None
        self.recording = None

    def recordMethod(self):
        if self.mic == None:
            self.buildWindow()
        else:
            self.setRecordIcon()

    def setRecordIcon(self, event=None):
        if self.mic != None:
            self.recordStopCount = (self.recordStopCount + 1) % 2
            if self.recordStopCount == 0:
                self.record.setIcon(QIcon(self.recordStopIcones[self.recordStopCount]))
                self.record.setStatusTip('Enregistrer')
                self.recording.quit()
            else:
                self.record.setIcon(QIcon(self.recordStopIcones[self.recordStopCount]))
                self.record.setStatusTip('Stoper')
                self.recording = TR(self.mic, self.setNotes, self.setFilePath)
                self.recording.start()

    def buildWindow(self):
        self.window = SelectAMicWindow(listMic(), self.setSelfMic, self.setRecordIcon)
        self.window.setWindowModality(Qt.WindowModal)
        self.window.setGeometry(100, 200, 100, 100)
        self.window.show()

    def setSelfMic(self, mic):
        self.mic = mic

class SelectAMicWindow(QWidget):
    def __init__(self, listMic, setSelfMic, setRecordIcon):
        QWidget.__init__(self)
        self.listMic = listMic
        self.initUI()
        self.setSelfMic = setSelfMic
        self.setRecordIcon = setRecordIcon

    def initUI(self):
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setGeometry(QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.listView = QListWidget(self)
        self.listView.setGeometry(QRect(30, 40, 331, 191))
        self.listView.setObjectName("listView")
        for i in self.listMic:
            self.listView.addItem(i['name'])
        self.label = QLabel(self)
        self.label.setGeometry(QRect(30, 10, 281, 16))
        self.label.setText('Choose a microphone :')
        self.setFixedSize(450, 320)
        self.label.setObjectName("label")

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.cancel)
        QMetaObject.connectSlotsByName(self)

    def accept(self):
        if self.listView.selectedItems() != []:
            self.setSelfMic(self.listView.selectedItems()[0].text())
            self.setRecordIcon()
        self.close()

    def cancel(self):
        self.close()