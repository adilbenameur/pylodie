#pylint: disable=no-name-in-module
#pylint: disable=no-member, import-error

#import PyQt dependencies
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtCore import Qt

#import other libs
import aubio
import pickle

#import Pylodie's dependencies
from pylodie.func.processingAudio.getBpm import getBpm


class TempoWidget(QWidget):
    def __init__(self, getTempo):
        super().__init__()
        # Fonction permettant d'obtenir le tempo
        self.getTempo = getTempo

    
    # Fonction permettant d'afficher le tempo
    def paintEvent(self, event):
        painter = QPainter(self)
        pen = QPen(Qt.black)
        pen.setWidth(1)
        painter.setPen(pen)

        font = painter.font()
        font.setPixelSize(28)
        painter.setFont(font)
        painter.drawText(0,35,"= {}".format(self.getTempo()))
        self.update()