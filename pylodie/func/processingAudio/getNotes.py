from subprocess import check_output
#pylint: disable=no-name-in-module
from aubio import notes as getNote, source, midi2note
import math


def truncateFloat(number, decimalNumber, doRound=True):
    if doRound:
        return math.ceil(number * 10 ** decimalNumber) / 10 ** decimalNumber
    else:
        return int(number * 10 ** decimalNumber) / 10 ** decimalNumber


def getNotes(filePath, midiToNotes=True, decimalNumber=6, printOutput=False):
    total_frames = 0
    src = source(filePath)
    notes = getNote()
    outpouted_notes = []
    while True:
        samples, read = src()
        new_note = notes(samples)
        if new_note[0] != 0:
            if midiToNotes:
                outpouted_notes.append([midi2note(int(float(new_note[0]))), truncateFloat(total_frames/float(src.samplerate), decimalNumber)])
            else:
                outpouted_notes.append([total_frames/float(src.samplerate), new_note[0]])
        total_frames += read
        if read < 256: break
    return outpouted_notes