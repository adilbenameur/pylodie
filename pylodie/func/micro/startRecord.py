import tempfile
import queue
import sounddevice as sd
import soundfile as sf
import soundfile
#pylint: disable=no-name-in-module
from PyQt5.QtCore import QThread
from subprocess import check_output
from aubio import notes as getNote, source, midi2note
import math
import time
from pylodie.func.processingAudio.getNotes import getNotes

class ThreadedRecording(QThread):
    def __init__(self, device, setNotes, setFilePath):
        QThread.__init__(self)
        self.tempFilePath = None
        self.device = device
        self.setNotes = setNotes
        self.setFilePath = setFilePath

    def quit(self):
        self.stream.close()
        self.file.close()
        self.setNotes(getNotes(self.tempFilePath))
        self.setFilePath(self.tempFilePath)
        self.exit()

    def run(self):
        q = queue.Queue()
        samplerate = int(sd.query_devices(self.device, 'input')['default_samplerate'])
        self.tempFilePath = str(tempfile.NamedTemporaryFile(mode="w", prefix='pylodie-', suffix='.wav').name)

        def callback(indata, frames, time, status):
            q.put(indata.copy())

        with soundfile.SoundFile(file=self.tempFilePath, mode='w+', samplerate=samplerate, channels=1, subtype=None, endian=None, format='WAV', closefd=False) as self.file:
            with sd.InputStream(samplerate=samplerate, device=self.device,
                                channels=1, callback=callback) as self.stream:
                while True:
                    data = q.get()
                    self.file.write(data)
                    self.file.flush()